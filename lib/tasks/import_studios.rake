# frozen_string_literal: true

module Studios
  # Handles the import.
  class Importer
    def perform(file_name)
      JSON.parse(File.read(file_name)).each do |data|
        data = data.transform_keys(&:underscore)
        model = Studio.create(data)
        puts("[STUDIO] #{model.name}")
        puts(" -> #{model.errors.full_messages.to_sentence}") if model.invalid?
      end
    end
  end
end

desc 'Import studios'
task import_studios: :environment do
  Studios::Importer.new.perform(Rails.root.join('db/data/studios.json'))
end
