# frozen_string_literal: true

Rails.application.routes.draw do
  scope defaults: { format: 'json' } do
    devise_for :users

    resource :database, only: %i[] do
      get :download, on: :collection
    end

    namespace :api do
      namespace :v1 do
        resources :list_entries, except: %i[new edit] do
          delete :clear, on: :collection
          get :statuses, on: :collection
        end
        resources :media, except: %i[new edit] do
          get :kinds, on: :collection
          get :statuses, on: :collection
        end
        resources :notifications, only: %i[index destroy]
        resource :profile, only: %i[show update]
        resources :reviews, except: %i[new edit]
        resources :status_posts, except: %i[new edit] do
          post :like, on: :member
          post :unlike, on: :member
        end
        resources :studios, except: %i[new edit]
        resources :tags, except: %i[new edit]
        resources :user_reports, only: %i[index create destroy]
        resources :users, except: %i[new edit create]

        namespace :list_entries do
          resource :import, only: %i[] do
            post :anilist, on: :collection
          end
        end
      end
    end
  end
end
