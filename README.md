GYARU: Gobusto's Yasashii Anime Recording Utility
=================================================

This is massively unfinished right now; I'll write more here later...

TODO:
-----

This is basically just for my own reference, so that I don't forget anything:

+ Implement a "following" system, so that status posts can be filtered.
+ Implement the ability to "block" accounts.
+ Implement websocket support (for real-time updates) via ActionCable.
+ Implement a recommendations system.
+ Implement a "related media" system.
+ Add support for characters/staff.
+ Send a confirmation email before allowing users to log in for the first time.
+ Lock accounts after too many unsuccessful log-in attempts?
+ Implement a real "forgot my password" email.
+ It might also be nice to add GraphQL support one day in the distant future.
+ Store links to streaming sites/etc. like some other tracking sites do?
+ Store links to trailers on YouTube?
+ Store an explicit TV/Movie/OVA value for media entries?
+ Return "more" instead of "total" for API index endpoints.
+ Implement sorting for API index endpoints.
+ Allow media to be filtered by "not on my list".
+ Return an error if expired tokens are sent to endpoints which don't need one.
+ Do not allow scores/episode counts for "ignored" or "planning" entries.
+ Setting the episode count to the total should (maybe) auto-mark as complete.
+ Setting a list item as planning/ignored should clear the "episode" count(s).
+ Store "started at" and "finished at" dates for list entries?
+ Add a "complete" user data export endpoint?
+ Add a "clear all notifications" endpoint.
+ Custom lists?
+ Direct messages?
+ Add an export endpoint for studios (not just media).

Also, this bug crops up sometimes: https://github.com/rails/rails/issues/37591

    ActiveRecord::AssociationTypeMismatch (User(#21980) expected, got
    #<User id: 8, email: "test@example.com", created_at: "2020-08-10 19:46:43",
    updated_at: "2020-08-16 10:16:29", name: "NotGoBusto", about: nil> which is
    an instance of User(#12940)):

Fortunately, it only seems to happen in development mode, so it's probably OK.

License
-------

Copyright (c) 2020 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
