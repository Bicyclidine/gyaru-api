# frozen_string_literal: true

# == Schema Information
#
# Table name: media_tags
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  media_id   :bigint
#  tag_id     :bigint
#
# Indexes
#
#  index_media_tags_on_media_id             (media_id)
#  index_media_tags_on_media_id_and_tag_id  (media_id,tag_id) UNIQUE
#  index_media_tags_on_tag_id               (tag_id)
#

# Association between a media entry and a tag.
class MediaTag < ApplicationRecord
  belongs_to :media
  belongs_to :tag

  validates :tag, uniqueness: { scope: :media_id }
end
