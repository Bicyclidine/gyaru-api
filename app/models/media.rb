# frozen_string_literal: true

# == Schema Information
#
# Table name: media
#
#  id                :bigint           not null, primary key
#  about             :text(65535)
#  adult             :boolean          default(FALSE), not null
#  anidb_code        :string(255)
#  anilist_code      :string(255)
#  anime_planet_code :string(255)
#  episodes          :integer
#  kind              :integer
#  kitsu_code        :string(255)
#  mal_code          :string(255)
#  status            :integer
#  volumes           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  studio_id         :bigint
#
# Indexes
#
#  index_media_on_studio_id  (studio_id)
#

# Anime, Manga, etc.
class Media < ApplicationRecord
  include WithNameScope

  enum kind: { anime: 0, manga: 1 }
  enum status: { unreleased: 0, releasing: 1, complete: 2, cancelled: 3 }

  has_one_attached :picture

  belongs_to :studio, optional: true
  has_many :list_entries, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :names, class_name: 'MediaName', dependent: :destroy

  has_many :media_tags, dependent: :destroy
  has_many :tags, through: :media_tags

  accepts_nested_attributes_for :names, allow_destroy: true
  accepts_nested_attributes_for :media_tags, allow_destroy: true

  validates :names, :kind, :status, presence: true

  # TODO: Episodes should be REQUIRED unless the media is currently unreleased.
  validates :episodes, numericality: { greater_than: 0 }, allow_blank: true
  validates :volumes, numericality: { greater_than: 0 }, allow_blank: true

  validates(
    :picture,
    size: { less_than: 3.megabytes },
    content_type: %w[image/png image/jpg image/jpeg image/gif]
  )

  # We need IDs (not URLs) for data import. Anime Planet is excluded for now...
  %i[anidb anilist kitsu mal].each do |site|
    validates("#{site}_code", numericality: true, allow_blank: true)
  end

  validates :anidb_code, absence: true, unless: :anime? # AniDB only has anime.
  validates :studio, absence: true, unless: :anime? # Doesn't apply for manga.
  validates :volumes, absence: true, unless: :manga? # Doesn't apply for anime.
end
