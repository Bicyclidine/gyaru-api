# frozen_string_literal: true

module Reviews
  # Generates JSON for a review.
  class JsonGenerator
    FIELDS = %w[id summary created_at updated_at].freeze

    def initialize(review)
      @review = review
    end

    def as_json(complete: false)
      result = @review.as_json.slice(*FIELDS).merge(
        'media' => ::Medias::JsonGenerator.new(@review.media).as_json,
        'user' => ::Users::JsonGenerator.new(@review.user).as_json
      )

      result['content'] = @review.content if complete

      result.transform_keys { |k| k.camelize(:lower) }
    end
  end
end
