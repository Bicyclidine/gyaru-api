# frozen_string_literal: true

# == Schema Information
#
# Table name: notifications
#
#  id           :bigint           not null, primary key
#  context_type :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  context_id   :bigint
#  user_id      :bigint
#
# Indexes
#
#  index_notifications_on_context_type_and_context_id  (context_type,context_id)
#  index_notifications_on_user_id                      (user_id)
#

# A notification.
class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :context, polymorphic: true

  validate :context_allowed, if: :context

  private

  def context_allowed
    return if context.class.included_modules.include?(NotificationContext)

    errors[:context] << "is not allowed: #{context.class}"
  end
end
