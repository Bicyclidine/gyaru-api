# frozen_string_literal: true

# == Schema Information
#
# Table name: user_reports
#
#  id             :bigint           not null, primary key
#  reason         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status_post_id :bigint
#  user_id        :bigint
#
# Indexes
#
#  index_user_reports_on_status_post_id              (status_post_id)
#  index_user_reports_on_user_id                     (user_id)
#  index_user_reports_on_user_id_and_status_post_id  (user_id,status_post_id) UNIQUE
#

# Represents a "report" from a user for the attention of administrators.
class UserReport < ApplicationRecord
  belongs_to :user
  belongs_to :status_post

  validates :status_post_id, uniqueness: { scope: :user_id }
  validates :reason, presence: true
end
