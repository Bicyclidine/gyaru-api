# frozen_string_literal: true

# Allows Media (or related records, such as ListEntry) to be sorted by name.
module WithNameScope
  extend ActiveSupport::Concern

  # Using .where() won't work; media without JP names would be filtered out!
  WITH_NAME_JOIN = %(
    LEFT JOIN media_names ON media_names.media_id = media.id
    AND media_names.language = 'jp'
  )

  included do
    # Scope: Uses the alphabetical-first name as a "default" for ordering, etc.
    def self.with_name
      media_model = table_name == 'media' # Special case: No joining-to-media.

      result = joins(WITH_NAME_JOIN) # Only join on MATCHING LANGUAGE records!
      result = result.left_outer_joins(media: :names) unless media_model
      result.select("#{table_name}.*, MIN(media_names.text) AS name").group(:id)
    end
  end
end
