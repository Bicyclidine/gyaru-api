# frozen_string_literal: true

# Provides functionality for linking models to observers.
module ObservableContext
  extend ActiveSupport::Concern

  included do
    def self.observed_by(klass)
      klass::EVENT_HOOKS.each do |e|
        public_send(e, -> { klass.new.public_send(e, self) if klass.enabled? })
      end
    end
  end
end
