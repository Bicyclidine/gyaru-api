# frozen_string_literal: true

# Allows records to generate notifications.
module NotificationContext
  extend ActiveSupport::Concern

  included do
    has_many :notifications, as: :context, dependent: :destroy
  end
end
