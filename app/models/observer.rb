# frozen_string_literal: true

# Observer base class.
class Observer
  EVENT_HOOKS = %i[
    after_create_commit
    after_update_commit
    after_destroy_commit
  ].freeze

  # Allow observers to be switched off (during unit tests, for example):
  class << self
    define_method(:enabled?) { defined?(@enabled) ? @enabled : true }
    define_method(:enable!) { @enabled = true }
    define_method(:disable!) { @enabled = false }
  end

  # Define default methods for each event, which subclasses can override:
  EVENT_HOOKS.each { |event| define_method(event) { |_model| } }
end
