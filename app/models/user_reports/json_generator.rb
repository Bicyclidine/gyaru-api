# frozen_string_literal: true

module UserReports
  # Generates JSON for a report.
  class JsonGenerator
    def initialize(user_report)
      @user_report = user_report
    end

    def as_json
      data = @user_report.as_json.except('user_id', 'status_post_id')
      data = data.merge('user' => user_json, 'statusPost' => status_post_json)
      data.transform_keys { |k| k.camelize(:lower) }
    end

    private

    def user_json
      Users::JsonGenerator.new(@user_report.user).as_json
    end

    def status_post_json
      status_post = @user_report.status_post
      StatusPosts::JsonGenerator.new(status_post).as_json(include_parent: true)
    end
  end
end
