# frozen_string_literal: true

module UserReports
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    # rubocop:disable Lint/UnusedMethodArgument
    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      UserReport.all.order(created_at: :desc)
    end
    # rubocop:enable Lint/UnusedMethodArgument
  end
end
