# frozen_string_literal: true

module ListEntries
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = general_filters(ListEntry.with_name, params)
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, search)
      records.where('name LIKE ?', "%#{search}%")
    end

    def sorted(records, _field, _direction)
      records.order(:name)
    end

    def general_filters(records, params)
      kind = params[:kind].presence
      user = params[:user].presence
      media = params[:media].presence
      status = params[:status].presence

      records = records.where(media: { kind: kind }) if kind
      records = records.where(user_id: user) if user
      records = records.where(media_id: media) if media
      records = records.where(status: status) if status
      records
    end
  end
end
