# frozen_string_literal: true

module ListEntries
  module Importers
    # Creates list entries from an Anilist "gdpr_data.json" file.
    class FromAnilistGdpr
      # Map the numbers from Anilist to an enum value for our ListEntry model:
      SERIES_TYPES = { 0 => 'anime', 1 => 'manga' }.freeze

      # Map the numbers from Anilist to an enum value for our ListEntry model:
      STATUSES = {
        0 => 'in_progress',
        1 => 'planning',
        2 => 'complete',
        3 => 'dropped',
        4 => 'paused'
      }.freeze

      def initialize(user)
        @user = user
      end

      def perform(file_data)
        return { 'error' => 'No file provided' } unless file_data.is_a?(String)

        items = JSON.parse(file_data)['lists'].map do |entry_json|
          kind = kind_from(entry_json)
          anilist_code = anilist_code_from(entry_json)
          media = Media.find_by(kind: kind, anilist_code: anilist_code)

          key = media ? media.names.first.text : "#{kind} #{anilist_code}"
          errors = media ? create_list_entry(media, entry_json) : ['Not in DB']
          [key, { kind: kind, errors: errors }]
        end

        Hash[items]
      end

      private

      def kind_from(entry_json)
        SERIES_TYPES[entry_json['series_type']]
      end

      def anilist_code_from(entry_json)
        entry_json['series_id'].to_s
      end

      def status_from(entry_json)
        STATUSES[entry_json['status']]
      end

      # NOTE: Scores, episodes, and volumes are given as integers, not `null`:
      def zero_as_null(entry_json, field)
        entry_json[field].positive? ? entry_json[field] : nil
      end

      # TODO: Also import Started/Finished date?
      def create_list_entry(media, entry_json)
        record = @user.list_entries.find_or_create_by(media: media) do |item|
          item.status = status_from(entry_json)
          item.score = zero_as_null(entry_json, 'score')
          item.episodes = zero_as_null(entry_json, 'progress')
          item.volumes = zero_as_null(entry_json, 'progress_volume')
          item.notes = entry_json['notes']
        end

        record.errors.full_messages
      end
    end
  end
end
