# frozen_string_literal: true

module Medias
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = general_filters(Media.with_name, params)
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, search)
      records.where('media_names.text LIKE ?', "%#{search}%")
    end

    def sorted(records, _field, _direction)
      records.order(:name)
    end

    def general_filters(records, params)
      adult = params[:adult].presence
      kind = params[:kind].presence
      status = params[:status].presence
      studio = params[:studio].presence

      records = records.where(adult: adult) if adult
      records = records.where(kind: kind) if kind
      records = records.where(status: status) if status
      records = records.where(studio_id: studio) if studio
      records
    end
  end
end
