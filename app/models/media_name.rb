# frozen_string_literal: true

# == Schema Information
#
# Table name: media_names
#
#  id         :bigint           not null, primary key
#  language   :string(255)
#  text       :string(255)
#  variant    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  media_id   :bigint
#
# Indexes
#
#  index_media_names_on_media_id           (media_id)
#  index_media_names_on_media_id_and_text  (media_id,text) UNIQUE
#

# A variant name for a media record.
class MediaName < ApplicationRecord
  ISO_639_1 = %w[en jp].freeze # TODO: Expand this later...

  # TODO: Handle other options, such as Cyrillic, or Korean Hangeul/Romaja.
  enum variant: { kana: 0, kanji: 1, romaji: 2 }

  belongs_to :media, inverse_of: :names

  before_validation -> { language&.downcase } # Ensure that "EN" becomes "en".
  validates :language, inclusion: { in: ISO_639_1 }

  validates :text, presence: true
  validate :text_uniqueness!

  validates :variant, presence: true, if: :jp?
  validates :variant, absence: true, unless: :jp?

  private

  def jp?
    language == 'jp'
  end

  # Special condition: The text only needs to be unique for this kind of media.
  def text_uniqueness!
    other_name = self.class.joins(:media).where(media: { kind: media&.kind })
    return unless other_name.where(text: text).where.not(id: id).exists?

    errors[:text] << I18n.t('taken', scope: 'errors.messages')
  end
end
