# frozen_string_literal: true

module Users
  # Generates JSON for a user.
  class JsonGenerator
    def initialize(user)
      @user = user
    end

    def as_json(current_user: nil, complete: false)
      result = basic_json
      result['email'] = @user.email if current_user == @user
      result['about'] = @user.about if complete
      result
    end

    private

    # NOTE: We can't use `@user.as_json` because of Devise - see `user.rb`.
    def basic_json
      {
        'id' => @user.id,
        'name' => @user.name,
        'avatar' => Uploads::JsonHelper.new(@user.avatar).public_url,
        'administrator' => @user.administrator?,
        'createdAt' => @user.created_at.iso8601(3),
        'updatedAt' => @user.updated_at.iso8601(3)
      }
    end
  end
end
