# frozen_string_literal: true

module StatusPosts
  # Generates notifications in response to specific events.
  class Notifier < Observer
    def after_create_commit(post)
      return unless post.parent && post.parent.user != post.user

      post.notifications.create!(user: post.parent.user)
    end
  end
end
