# frozen_string_literal: true

module StatusPosts
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = StatusPost.where(parent: nil)
      records = records.where(user_id: params[:user]) if params[:user].present?
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, search)
      records.where('name LIKE ?', "%#{search}%")
    end

    def sorted(records, _field, _direction)
      records.order(created_at: :desc)
    end
  end
end
