# frozen_string_literal: true

# == Schema Information
#
# Table name: likes
#
#  id             :bigint           not null, primary key
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  status_post_id :bigint
#  user_id        :bigint
#
# Indexes
#
#  index_likes_on_status_post_id              (status_post_id)
#  index_likes_on_status_post_id_and_user_id  (status_post_id,user_id) UNIQUE
#  index_likes_on_user_id                     (user_id)
#

# A "like" on a status post.
class Like < ApplicationRecord
  include NotificationContext
  include ObservableContext

  observed_by Likes::Notifier

  belongs_to :user
  belongs_to :status_post

  validates :user, uniqueness: { scope: :status_post_id }
end
