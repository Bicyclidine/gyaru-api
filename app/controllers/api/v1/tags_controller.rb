# frozen_string_literal: true

module Api
  module V1
    # Tag CRUD.
    class TagsController < BaseController
      before_action :administrators_only!, only: %i[create update destroy]

      def index
        simple_index(Tags::Filter) { |m| json_for(m) }
      end

      def show
        tag = Tag.find(params[:id])
        render(json: json_for(tag))
      end

      def create
        simple_create(Tag, tag_params) { |tag| json_for(tag) }
      end

      def update
        tag = Tag.find(params[:id])
        simple_update(tag, tag_params) { json_for(tag) }
      end

      def destroy
        tag = Tag.find(params[:id])
        simple_destroy(tag)
      end

      private

      def tag_params
        params.require(:tag).permit(:name, :description)
      end

      def json_for(tag)
        Tags::JsonGenerator.new(tag).as_json
      end
    end
  end
end
