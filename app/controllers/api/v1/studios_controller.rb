# frozen_string_literal: true

module Api
  module V1
    # Studio CRUD.
    class StudiosController < BaseController
      before_action :administrators_only!, only: %i[create update destroy]

      def index
        simple_index(Studios::Filter) { |m| json_for(m, complete: false) }
      end

      def show
        studio = Studio.find(params[:id])
        render(json: json_for(studio))
      end

      def create
        simple_create(Studio, studio_params) { |studio| json_for(studio) }
      end

      def update
        studio = Studio.find(params[:id])
        simple_update(studio, studio_params) { json_for(studio) }
      end

      def destroy
        studio = Studio.find(params[:id])
        simple_destroy(studio)
      end

      private

      DIRECT_ATTRIBUTES = %i[
        name
        about
        logo
        anidbCode
        anilistCode
        animePlanetCode
        kitsuCode
        malCode
      ].freeze

      def studio_params
        params.require(:studio).permit(*DIRECT_ATTRIBUTES)
      end

      def json_for(studio, complete: true)
        Studios::JsonGenerator.new(studio).as_json(complete: complete)
      end
    end
  end
end
