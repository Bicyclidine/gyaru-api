# frozen_string_literal: true

module Api
  module V1
    module ListEntries
      # Handles import of list data from other sites.
      class ImportsController < ::Api::V1::BaseController
        before_action :members_only!

        def anilist
          handler = ::ListEntries::Importers::FromAnilistGdpr.new(current_user)
          results = handler.perform(file_data_from(params[:gdpr]))
          render(status: results.key?('error') ? 400 : 200, json: results)
        end

        private

        def file_data_from(data)
          data.is_a?(ActionDispatch::Http::UploadedFile) ? data.read : data
        end
      end
    end
  end
end
