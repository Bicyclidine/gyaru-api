# frozen_string_literal: true

module Api
  module V1
    # Logic for editing your user profile i.e. anything not handled by Devise.
    class ProfilesController < BaseController
      before_action :members_only!

      def show
        render(json: json_for_user)
      end

      def update
        simple_update(current_user, user_params) { json_for_user }
      end

      private

      def user_params
        params.require(:user).permit(:name, :about, :avatar)
      end

      def json_for_user
        generator = Users::JsonGenerator.new(current_user)
        generator.as_json(current_user: current_user, complete: true)
      end
    end
  end
end
