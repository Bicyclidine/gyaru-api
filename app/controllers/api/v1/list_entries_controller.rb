# frozen_string_literal: true

module Api
  module V1
    # Anime and Manga.
    class ListEntriesController < BaseController
      before_action :members_only!, except: %i[index show statuses]

      def index
        attrs = {
          kind: params[:kind],
          user: params[:user],
          media: params[:media],
          status: params[:status]
        }

        simple_index(::ListEntries::Filter, **attrs) { |entry| json_for(entry) }
      end

      def show
        list_entry = ListEntry.find(params[:id])
        render(json: json_for(list_entry))
      end

      def create
        create_params = list_entry_params.merge(user: current_user)
        simple_create(ListEntry, create_params) { |record| json_for(record) }
      end

      def update
        list_entry = current_user.list_entries.find(params[:id])
        simple_update(list_entry, list_entry_params) { json_for(list_entry) }
      end

      def destroy
        list_entry = current_user.list_entries.find(params[:id])
        simple_destroy(list_entry)
      end

      def clear
        kind = params[:kind].presence

        entries = current_user.list_entries
        entries = entries.joins(:media).where(media: { kind: kind }) if kind
        entries.destroy_all

        head(204)
      end

      def statuses
        render(json: ListEntry.statuses.keys)
      end

      private

      def list_entry_params
        attrs = %i[mediaId status score episodes volumes notes]
        params.require(:listEntry).permit(*attrs)
      end

      def json_for(list_entry)
        ::ListEntries::JsonGenerator.new(list_entry).as_json
      end
    end
  end
end
