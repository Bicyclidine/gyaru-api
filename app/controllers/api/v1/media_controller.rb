# frozen_string_literal: true

module Api
  module V1
    # Anime and Manga.
    class MediaController < BaseController
      before_action :administrators_only!, only: %i[create update destroy]

      def index
        attrs = {
          adult: params[:adult],
          kind: params[:kind],
          status: params[:status],
          studio: params[:studio]
        }

        simple_index(Medias::Filter, **attrs) { |m| json_for(m) }
      end

      def show
        media = Media.find(params[:id])
        render(json: json_for(media, complete: true))
      end

      def create
        simple_create(Media, media_params) do |media|
          json_for(media, complete: true)
        end
      end

      def update
        media = Media.find(params[:id])
        simple_update(media, media_params) { json_for(media, complete: true) }
      end

      def destroy
        media = Media.find(params[:id])
        simple_destroy(media)
      end

      def kinds
        render(json: Media.kinds.keys)
      end

      def statuses
        render(json: Media.statuses.keys)
      end

      private

      DIRECT_ATTRIBUTES = %i[
        kind
        about
        adult
        picture
        anidbCode
        anilistCode
        animePlanetCode
        kitsuCode
        malCode
        status
        episodes
        volumes
        studioId
      ].freeze

      def media_params
        params.require(:media).permit(
          *DIRECT_ATTRIBUTES,
          namesAttributes: %i[id text language variant _destroy].freeze,
          mediaTagsAttributes: %i[id tagId _destroy].freeze
        )
      end

      def json_for(media, complete: false)
        Medias::JsonGenerator.new(media).as_json(complete: complete)
      end
    end
  end
end
