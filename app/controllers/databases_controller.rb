# frozen_string_literal: true

# Export the database. Not part of the API; this is a "special" action.
class DatabasesController < ActionController::API
  # TODO: Possibly cache this, rather than re-generating for each request?
  def download
    json = Medias::Exporter.new.as_json
    send_data(json.to_json, filename: 'media.json')
  end
end
