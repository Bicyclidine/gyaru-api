# frozen_string_literal: true

# This is the base controller class. Devise derives its controllers from this.
class ApplicationController < ActionController::API
  include ActionController::MimeResponds # For Devise JWT.

  respond_to :json # For Devise JWT.

  # See https://stackoverflow.com/a/42572759/4200092
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name])
  end
end
