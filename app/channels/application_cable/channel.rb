# frozen_string_literal: true

module ApplicationCable
  # Base channel class.
  class Channel < ActionCable::Channel::Base
  end
end
