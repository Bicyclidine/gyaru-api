# frozen_string_literal: true

module ApplicationCable
  # Base connection class.
  class Connection < ActionCable::Connection::Base
  end
end
