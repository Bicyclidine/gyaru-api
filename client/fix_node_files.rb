# frozen_string_literal: true

REACT = 'lib/react.development.js'

def replace_line(file_name, line)
  top = '../' * (file_name.split('/').count - 2)

  return line unless line.start_with?('import ')
  return "/* CSS import omitted */\n" if line.include?('.css') # NodeJS only :(
  return "import React from '#{top}#{REACT}'\n" if line.include?('import React')
  return "import '#{top}lib/commonmark.js'\n" if line.include?('commonmark')

  line
end

def fix_imports(file_name)
  text = File.read(file_name)
  text = text.lines.map { |line| replace_line(file_name, line) }.join
  File.open(file_name, 'w') { |file| file.write(text) }
end

# Fix "import" lines which differ between this version and the NodeJS one:
misc = Dir['./src/*.js']
pages = Dir['./src/pages/*/*.js']
components = Dir['./src/components/*/*.js']
(misc + pages + components).each { |file_name| fix_imports(file_name) }

# Delete Jest test files:
`rm -Rf ./src/*.test.js*`
`rm -Rf ./src/*/*.test.js*`
`rm -Rf ./src/*/*/*.test.js*`

# Delete Jest snapshots:
`rm -Rf ./src/__snapshots__`
`rm -Rf ./src/*/__snapshots__`
`rm -Rf ./src/*/*/__snapshots__`

# Delete Node-specific files, and restore our custom `index.js` file:
`rm ./src/serviceWorker.js`
`rm ./src/setupTests.js`
`git checkout -- ./src/index.js`
