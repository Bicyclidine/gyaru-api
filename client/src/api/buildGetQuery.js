// Build an URLSearchParams object from a JSON-style object... if one is given!
function buildGetQuery (params) {
  const data = new URLSearchParams()
  Object.keys(params || {}).forEach(key => data.append(key, params[key]))
  return data
}

export default buildGetQuery
