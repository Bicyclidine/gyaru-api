// Contains various enums which are unlikely to change, to avoid API requests.

const mediaKinds = ['anime', 'manga']

const mediaStatuses = ['unreleased', 'releasing', 'complete', 'cancelled']

const entryStatuses = [
  'planning',
  'in_progress',
  'paused',
  'complete',
  'dropped',
  'ignored'
]

export { mediaKinds, mediaStatuses, entryStatuses }
