import React from '../lib/react.development.js'
import ReactDOM from '../lib/react-dom.development.js'

import App from './App.js'

window.process = {
  env: {
    REACT_APP_API_SERVER: 'http://127.0.0.1:3000',
    REACT_APP_PATHLESS_ROUTING: true
  }
}

window.onload = function () {
  ReactDOM.render(
    React.createElement(App),
    document.querySelector('#root')
  )
}
