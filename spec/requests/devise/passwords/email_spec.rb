# frozen_string_literal: true

RSpec.describe 'Devise: Password-Reset Email', type: :request do
  it 'returns an empty JSON response (and sends an email) on success' do
    create(:user, email: 'test@example.com')

    params = { user: { email: 'test@example.com' } }
    post '/users/password', xhr: true, params: params

    expect(response.body).to eq('{}')
    expect(response.status).to eq(201)
    expect(ActionMailer::Base.deliveries.size).to eq(1)

    email = ActionMailer::Base.deliveries.first
    expect(email.subject).to eq('Reset password instructions')
    expect(email.body.raw_source).to include('edit?reset_password_token=')

    expect(email.reply_to).to eq(%w[thomas@kiniro.uk])
    expect(email.from).to eq(%w[thomas@kiniro.uk])
    expect(email.sender).to eq(nil)

    expect(email.to).to eq(%w[test@example.com])
    expect(email.cc).to eq(nil)
    expect(email.bcc).to eq(nil)
  end

  it 'returns an error message if the email address is unrecognised' do
    params = { user: { email: 'test@example.com' } }
    post '/users/password', xhr: true, params: params

    expected_json = {
      'errors' => {
        'email' => ['not found']
      }
    }

    expect(JSON.parse(response.body)).to eq(expected_json)
    expect(response.status).to eq(422)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end

  it 'returns an error message if no email address is specified' do
    post '/users/password', xhr: true

    expected_json = {
      'errors' => {
        'email' => ["can't be blank"]
      }
    }

    expect(JSON.parse(response.body)).to eq(expected_json)
    expect(response.status).to eq(422)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end
end
