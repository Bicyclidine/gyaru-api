# frozen_string_literal: true

RSpec.describe 'Database: Special actions', type: :request do
  context '.download' do
    it 'returns the database contents in an easily-importable JSON format' do
      create(
        :media,
        kind: 'manga',
        status: 'cancelled',
        episodes: 36,
        volumes: 4,
        about: 'Description text goes here.',
        kitsu_code: '456',
        mal_code: '789',
        names_attributes: [
          { language: 'jp', variant: 'kanji', text: '漫画' }
        ]
      )

      expected_json = [
        {
          'kind' => 'manga',
          'status' => 'cancelled',
          'episodes' => 36,
          'volumes' => 4,
          'about' => 'Description text goes here.',
          'kitsuCode' => '456',
          'malCode' => '789',
          'namesAttributes' => [
            { 'language' => 'jp', 'variant' => 'kanji', 'text' => '漫画' }
          ]
        }
      ]

      get '/database/download'
      expect(response.header['Content-Disposition']).to include('"media.json"')
      expect(response.status).to eq(200)

      actual_json = JSON.parse(response.body)
      expect(actual_json).to eq(expected_json)
    end
  end
end
