# frozen_string_literal: true

RSpec.describe 'API V1: Tag', type: :request do
  context 'index' do
    it 'returns a list of results' do
      tag = create(:tag, name: 'GAINAX')

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => tag.id,
            'name' => 'GAINAX',
            'description' => nil,
            'createdAt' => tag.created_at.iso8601(3),
            'updatedAt' => tag.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/tags'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      tag = create(:tag, name: 'GAINAX')

      expected_json = {
        'id' => tag.id,
        'name' => 'GAINAX',
        'description' => nil,
        'createdAt' => tag.created_at.iso8601(3),
        'updatedAt' => tag.updated_at.iso8601(3)
      }

      get "/api/v1/tags/#{tag.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find Tag with 'id'=123"
      }

      get '/api/v1/tags/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      params = {
        tag: { name: 'GAINAX' }
      }

      post '/api/v1/tags', xhr: true, params: params
      tag = Tag.last

      expected_json = {
        'id' => tag.id,
        'name' => 'GAINAX',
        'description' => nil,
        'createdAt' => tag.created_at.iso8601(3),
        'updatedAt' => tag.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      tag = create(:tag, name: 'GAINAX')

      params = {
        tag: { description: 'Hello!' }
      }

      patch "/api/v1/tags/#{tag.id}", xhr: true, params: params
      tag.reload

      expected_json = {
        'id' => tag.id,
        'name' => 'GAINAX',
        'description' => 'Hello!',
        'createdAt' => tag.created_at.iso8601(3),
        'updatedAt' => tag.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user, administrator: true)
      sign_in(user)

      tag = create(:tag)

      expect(Tag.count).to eq(1)

      delete "/api/v1/tags/#{tag.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Tag.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
