# frozen_string_literal: true

RSpec.describe 'API V1: Media', type: :request do
  context 'index' do
    it 'returns a list of results' do
      media = create(
        :media,
        kind: 'anime',
        about: 'Kawaii',
        names_attributes: [
          { text: 'K-On!', language: 'jp', variant: 'romaji' }
        ]
      )

      media_name = media.names.first

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => media.id,
            'names' => [
              {
                'id' => media_name.id,
                'text' => 'K-On!',
                'language' => 'jp',
                'variant' => 'romaji',
                'createdAt' => media_name.created_at.iso8601(3),
                'updatedAt' => media_name.updated_at.iso8601(3)
              }
            ],
            'adult' => false,
            'kind' => 'anime',
            'status' => 'unreleased',
            'episodes' => nil,
            'picture' => nil,
            'createdAt' => media.created_at.iso8601(3),
            'updatedAt' => media.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/media'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      media = create(
        :media,
        kind: 'anime',
        about: 'Kawaii',
        names_attributes: [
          { text: 'K-On!', language: 'jp', variant: 'romaji' }
        ]
      )

      media_name = media.names.first

      expected_json = {
        'id' => media.id,
        'anidbCode' => nil,
        'anilistCode' => nil,
        'animePlanetCode' => nil,
        'kitsuCode' => nil,
        'malCode' => nil,
        'names' => [
          {
            'id' => media_name.id,
            'text' => 'K-On!',
            'language' => 'jp',
            'variant' => 'romaji',
            'createdAt' => media_name.created_at.iso8601(3),
            'updatedAt' => media_name.updated_at.iso8601(3)
          }
        ],
        'adult' => false,
        'kind' => 'anime',
        'status' => 'unreleased',
        'episodes' => nil,
        'picture' => nil,
        'about' => 'Kawaii',
        'studio' => nil,
        'mediaTags' => [],
        'createdAt' => media.created_at.iso8601(3),
        'updatedAt' => media.updated_at.iso8601(3)
      }

      get "/api/v1/media/#{media.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find Media with 'id'=123"
      }

      get '/api/v1/media/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      params = {
        media: {
          kind: 'anime',
          status: 'complete',
          namesAttributes: [
            { text: 'K-On!', language: 'jp', variant: 'romaji' }
          ]
        }
      }

      post '/api/v1/media', xhr: true, params: params

      media = Media.last
      media_name = media.names.first

      expected_json = {
        'id' => media.id,
        'anidbCode' => nil,
        'anilistCode' => nil,
        'animePlanetCode' => nil,
        'kitsuCode' => nil,
        'malCode' => nil,
        'names' => [
          {
            'id' => media_name.id,
            'text' => 'K-On!',
            'language' => 'jp',
            'variant' => 'romaji',
            'createdAt' => media_name.created_at.iso8601(3),
            'updatedAt' => media_name.updated_at.iso8601(3)
          }
        ],
        'adult' => false,
        'kind' => 'anime',
        'status' => 'complete',
        'episodes' => nil,
        'about' => nil,
        'picture' => nil,
        'studio' => nil,
        'mediaTags' => [],
        'createdAt' => media.created_at.iso8601(3),
        'updatedAt' => media.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      media = create(
        :media,
        kind: 'anime',
        about: 'Kawaii',
        names_attributes: [
          { text: 'K-On!', language: 'jp', variant: 'romaji' }
        ]
      )

      media_name = media.names.first

      studio = create(:studio, name: 'Kyoto Animation')

      params = {
        media: { studioId: studio.id }
      }

      patch "/api/v1/media/#{media.id}", xhr: true, params: params
      media.reload

      expected_json = {
        'id' => media.id,
        'anidbCode' => nil,
        'anilistCode' => nil,
        'animePlanetCode' => nil,
        'kitsuCode' => nil,
        'malCode' => nil,
        'names' => [
          {
            'id' => media_name.id,
            'text' => 'K-On!',
            'language' => 'jp',
            'variant' => 'romaji',
            'createdAt' => media_name.created_at.iso8601(3),
            'updatedAt' => media_name.updated_at.iso8601(3)
          }
        ],
        'adult' => false,
        'kind' => 'anime',
        'status' => 'unreleased',
        'episodes' => nil,
        'about' => 'Kawaii',
        'picture' => nil,
        'studio' => {
          'id' => studio.id,
          'name' => 'Kyoto Animation',
          'logo' => nil,
          'createdAt' => studio.created_at.iso8601(3),
          'updatedAt' => studio.updated_at.iso8601(3)
        },
        'mediaTags' => [],
        'createdAt' => media.created_at.iso8601(3),
        'updatedAt' => media.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user, administrator: true)
      sign_in(user)

      media = create(:media)

      expect(Media.count).to eq(1)

      delete "/api/v1/media/#{media.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Media.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'kinds' do
    it 'returns a list of possible enum values' do
      expected_json = %w[anime manga]

      get '/api/v1/media/kinds'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'statuses' do
    it 'returns a list of possible enum values' do
      expected_json = %w[unreleased releasing complete cancelled]

      get '/api/v1/media/statuses'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end
end
