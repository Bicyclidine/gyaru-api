# frozen_string_literal: true

RSpec.describe 'API V1: Media - Tags', type: :request do
  it 'allows new tags to be added' do
    user = create(:user, administrator: true)
    sign_in(user)

    media = create(:media)
    tag = create(:tag, name: '4koma')

    params = {
      media: {
        mediaTagsAttributes: [{ tagId: tag.id }]
      }
    }

    patch "/api/v1/media/#{media.id}", xhr: true, params: params
    media_tag = MediaTag.last

    expected_json = [
      {
        'id' => media_tag.id,
        'tag' => {
          'id' => tag.id,
          'name' => '4koma',
          'description' => nil,
          'createdAt' => tag.created_at.iso8601(3),
          'updatedAt' => tag.updated_at.iso8601(3)
        },
        'createdAt' => media_tag.created_at.iso8601(3),
        'updatedAt' => media_tag.updated_at.iso8601(3)
      }
    ]

    expect(JSON.parse(response.body)['mediaTags']).to eq(expected_json)
    expect(response.status).to eq(200)
  end

  it 'allows existing tags to be updated' do
    pending 'TODO'
    raise NotImplementedError, 'TODO'
  end

  it 'allows old tags to be removed' do
    user = create(:user, administrator: true)
    sign_in(user)

    media_tag = create(:media_tag)

    params = {
      media: {
        mediaTagsAttributes: [{ id: media_tag.id, _destroy: true }]
      }
    }

    patch "/api/v1/media/#{media_tag.media_id}", xhr: true, params: params
    expect(JSON.parse(response.body)['mediaTags']).to eq([])
    expect(response.status).to eq(200)
  end
end
