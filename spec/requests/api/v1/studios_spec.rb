# frozen_string_literal: true

RSpec.describe 'API V1: Studio', type: :request do
  context 'index' do
    it 'returns a list of results' do
      studio = create(:studio, name: 'GAINAX')

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => studio.id,
            'name' => 'GAINAX',
            'logo' => nil,
            'createdAt' => studio.created_at.iso8601(3),
            'updatedAt' => studio.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/studios'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      studio = create(:studio, name: 'GAINAX')

      expected_json = {
        'id' => studio.id,
        'name' => 'GAINAX',
        'logo' => nil,
        'about' => nil,
        'anidbCode' => nil,
        'anilistCode' => nil,
        'animePlanetCode' => nil,
        'kitsuCode' => nil,
        'malCode' => nil,
        'createdAt' => studio.created_at.iso8601(3),
        'updatedAt' => studio.updated_at.iso8601(3)
      }

      get "/api/v1/studios/#{studio.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find Studio with 'id'=123"
      }

      get '/api/v1/studios/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      params = {
        studio: { name: 'GAINAX' }
      }

      post '/api/v1/studios', xhr: true, params: params
      studio = Studio.last

      expected_json = {
        'id' => studio.id,
        'name' => 'GAINAX',
        'logo' => nil,
        'about' => nil,
        'anidbCode' => nil,
        'anilistCode' => nil,
        'animePlanetCode' => nil,
        'kitsuCode' => nil,
        'malCode' => nil,
        'createdAt' => studio.created_at.iso8601(3),
        'updatedAt' => studio.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      studio = create(:studio, name: 'GAINAX')

      params = {
        studio: { name: 'MAPPA' }
      }

      patch "/api/v1/studios/#{studio.id}", xhr: true, params: params
      studio.reload

      expected_json = {
        'id' => studio.id,
        'name' => 'MAPPA',
        'logo' => nil,
        'about' => nil,
        'anidbCode' => nil,
        'anilistCode' => nil,
        'animePlanetCode' => nil,
        'kitsuCode' => nil,
        'malCode' => nil,
        'createdAt' => studio.created_at.iso8601(3),
        'updatedAt' => studio.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user, administrator: true)
      sign_in(user)

      studio = create(:studio)

      expect(Studio.count).to eq(1)

      delete "/api/v1/studios/#{studio.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Studio.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
