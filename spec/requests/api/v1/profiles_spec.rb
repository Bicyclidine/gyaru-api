# frozen_string_literal: true

RSpec.describe 'API V1: Profiles', type: :request do
  context 'show' do
    it 'returns JSON for the current user' do
      user = create(:user, name: 'Steve', email: 'test@example.com')
      sign_in(user)

      get '/api/v1/profile', xhr: true

      expected_json = {
        'id' => user.id,
        'email' => 'test@example.com',
        'name' => 'Steve',
        'avatar' => nil,
        'administrator' => false,
        'about' => nil,
        'createdAt' => user.created_at.iso8601(3),
        'updatedAt' => user.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it "returns an error if you're not logged in" do
      expected_json = {
        'error' => 'You are not logged in'
      }

      get '/api/v1/profile', xhr: true
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(401)
    end
  end

  context 'update' do
    it 'can change your name' do
      user = create(:user, name: 'Steve', email: 'test@example.com')
      sign_in(user)

      params = { user: { name: 'David' } }
      patch '/api/v1/profile', xhr: true, params: params
      user.reload

      expected_json = {
        'id' => user.id,
        'email' => 'test@example.com',
        'name' => 'David',
        'avatar' => nil,
        'administrator' => false,
        'about' => nil,
        'createdAt' => user.created_at.iso8601(3),
        'updatedAt' => user.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'can change your "about" text' do
      user = create(:user, name: 'Steve', email: 'test@example.com')
      sign_in(user)

      params = { user: { about: 'I like **Markdown** text!' } }
      patch '/api/v1/profile', xhr: true, params: params
      user.reload

      expected_json = {
        'id' => user.id,
        'email' => 'test@example.com',
        'name' => 'Steve',
        'avatar' => nil,
        'administrator' => false,
        'about' => 'I like **Markdown** text!',
        'createdAt' => user.created_at.iso8601(3),
        'updatedAt' => user.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'can change your avatar' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      params = {
        user: { name: 'Steve' }
      }

      expected_json = {
        'error' => 'You are not logged in'
      }

      patch '/api/v1/profile', xhr: true, params: params
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(401)
    end
  end
end
