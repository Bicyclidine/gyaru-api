# frozen_string_literal: true

RSpec.describe 'API V1: User reports', type: :request do
  context 'index' do
    it 'returns a list of results' do
      user = create(:user, administrator: true)
      sign_in(user)

      good_user = create(:user, email: 'good@example.com', name: 'GoodBoy')
      bad_user = create(:user, email: 'bad@example.com', name: 'BadBoy')
      status_post = create(:status_post, user: bad_user, content: 'hewwo')

      user_report = create(
        :user_report,
        user: good_user,
        status_post: status_post,
        reason: 'RSpec example'
      )

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => user_report.id,
            'reason' => 'RSpec example',
            'user' => {
              'id' => good_user.id,
              'name' => 'GoodBoy',
              'avatar' => nil,
              'administrator' => false,
              'createdAt' => good_user.created_at.iso8601(3),
              'updatedAt' => good_user.updated_at.iso8601(3)
            },
            'statusPost' => {
              'id' => status_post.id,
              'parentId' => nil,
              'content' => 'hewwo',
              'adult' => false,
              'spoiler' => false,
              'user' => {
                'id' => bad_user.id,
                'name' => 'BadBoy',
                'avatar' => nil,
                'administrator' => false,
                'createdAt' => bad_user.created_at.iso8601(3),
                'updatedAt' => bad_user.updated_at.iso8601(3)
              },
              'liked' => false,
              'likeCount' => 0,
              'replyCount' => 0,
              'createdAt' => status_post.created_at.iso8601(3),
              'updatedAt' => status_post.updated_at.iso8601(3)
            },
            'createdAt' => user_report.created_at.iso8601(3),
            'updatedAt' => user_report.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/user_reports'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      good_user = create(:user, email: 'good@example.com', name: 'GoodBoy')
      sign_in(good_user)

      bad_user = create(:user, email: 'bad@example.com', name: 'BadBoy')
      status_post = create(:status_post, user: bad_user, content: 'hewwo')

      params = {
        userReport: { statusPostId: status_post.id, reason: 'RSpec example' }
      }

      post '/api/v1/user_reports', xhr: true, params: params
      user_report = UserReport.last

      expected_json = {
        'id' => user_report.id,
        'reason' => 'RSpec example',
        'user' => {
          'id' => good_user.id,
          'name' => 'GoodBoy',
          'avatar' => nil,
          'administrator' => false,
          'createdAt' => good_user.created_at.iso8601(3),
          'updatedAt' => good_user.updated_at.iso8601(3)
        },
        'statusPost' => {
          'id' => status_post.id,
          'parentId' => nil,
          'content' => 'hewwo',
          'adult' => false,
          'spoiler' => false,
          'user' => {
            'id' => bad_user.id,
            'name' => 'BadBoy',
            'avatar' => nil,
            'administrator' => false,
            'createdAt' => bad_user.created_at.iso8601(3),
            'updatedAt' => bad_user.updated_at.iso8601(3)
          },
          'liked' => false,
          'likeCount' => 0,
          'replyCount' => 0,
          'createdAt' => status_post.created_at.iso8601(3),
          'updatedAt' => status_post.updated_at.iso8601(3)
        },
        'createdAt' => user_report.created_at.iso8601(3),
        'updatedAt' => user_report.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user, administrator: true)
      sign_in(user)

      user_report = create(:user_report)

      expect(UserReport.count).to eq(1)

      delete "/api/v1/user_reports/#{user_report.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(UserReport.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
