# frozen_string_literal: true

RSpec.describe 'Media Tag', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      media_tag = build(:media_tag)
      media_tag.valid?
      expect(media_tag.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'Media must exist',
        'Tag must exist'
      ]

      media_tag = MediaTag.new
      expect(media_tag.valid?).to eq(false)
      expect(media_tag.errors.full_messages).to eq(errors)
    end

    it 'must have a unique tag/media combination' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
