# frozen_string_literal: true

RSpec.describe 'Notification', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      notification = build(:notification)
      notification.valid?
      expect(notification.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        'Context must exist'
      ]

      notification = Notification.new
      expect(notification.valid?).to eq(false)
      expect(notification.errors.full_messages).to eq(errors)
    end

    it 'must have a valid context' do
      error = 'Context is not allowed: Tag'

      user = User.new
      good = Notification.new(user: user, context: Like.new)
      fine = Notification.new(user: user, context: StatusPost.new)
      what = Notification.new(user: user, context: Tag.new)

      expect(good.tap(&:valid?).errors.full_messages).to eq([])
      expect(fine.tap(&:valid?).errors.full_messages).to eq([])
      expect(what.tap(&:valid?).errors.full_messages).to eq([error])
    end
  end
end
