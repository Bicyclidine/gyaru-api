# frozen_string_literal: true

RSpec.describe 'Status Posts: Notifier', type: :model do
  it 'creates a notification if a reply is added to a post' do
    status_post = create(:status_post)
    user = create(:user)
    StatusPosts::Notifier.enable!

    expect(Notification.count).to eq(0)
    reply = status_post.replies.create!(user: user, content: 'Foo')
    expect(Notification.count).to eq(1)

    notification = Notification.last
    expect(notification.user).to eq(status_post.user)
    expect(notification.context).to eq(reply)
  end

  it 'does not create a notification if the OP replied to their own post' do
    status_post = create(:status_post)
    StatusPosts::Notifier.enable!

    expect(Notification.count).to eq(0)
    status_post.replies.create!(user: status_post.user, content: 'Foo')
    expect(Notification.count).to eq(0)
  end

  it 'does not create a notification for non-replies (new threads)' do
    user = create(:user)
    StatusPosts::Notifier.enable!

    expect(Notification.count).to eq(0)
    StatusPost.create!(user: user, content: 'Foo')
    expect(Notification.count).to eq(0)
  end
end
