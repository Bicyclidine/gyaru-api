# frozen_string_literal: true

RSpec.describe 'Media', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      media = build(:media)
      media.valid?
      expect(media.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Names can't be blank",
        "Kind can't be blank",
        "Status can't be blank"
      ]

      media = Media.new
      expect(media.valid?).to eq(false)
      expect(media.errors.full_messages).to eq(errors)
    end
  end

  context 'name' do
    it 'must be unique within the scope of a specific kind of media' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can be re-used if the other use is for a different kind of media' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
