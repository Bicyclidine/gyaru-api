# frozen_string_literal: true

RSpec.describe 'Status Post', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      status_post = build(:status_post)
      status_post.valid?
      expect(status_post.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        "Content can't be blank"
      ]

      status_post = StatusPost.new
      expect(status_post.valid?).to eq(false)
      expect(status_post.errors.full_messages).to eq(errors)
    end
  end

  context 'replies' do
    it 'allows replies to "parent-less" status posts' do
      thread = StatusPost.new(content: 'Foo', user: User.new)
      reply = thread.replies.build(content: 'Bar', user: User.new)

      reply.valid?
      expect(reply.errors.full_messages).to eq([])
    end

    it 'rejects replies-to-replies' do
      thread = StatusPost.new(content: 'Foo', user: User.new)
      reply = thread.replies.build(content: 'Bar', user: User.new)
      extra = reply.replies.build(content: 'Baz', user: User.new)

      extra.valid?
      expect(extra.errors.full_messages).to eq(['Parent must be blank'])
    end
  end
end
