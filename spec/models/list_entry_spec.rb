# frozen_string_literal: true

RSpec.describe 'List Entry', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      list_entry = build(:list_entry)
      list_entry.valid?
      expect(list_entry.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        'Media must exist',
        "Status can't be blank"
      ]

      list_entry = ListEntry.new
      expect(list_entry.valid?).to eq(false)
      expect(list_entry.errors.full_messages).to eq(errors)
    end

    it 'must have a unique user/media combination' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
