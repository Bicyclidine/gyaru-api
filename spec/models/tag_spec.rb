# frozen_string_literal: true

RSpec.describe 'Tag', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      tag = build(:tag)
      tag.valid?
      expect(tag.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Name can't be blank"
      ]

      tag = Tag.new
      expect(tag.valid?).to eq(false)
      expect(tag.errors.full_messages).to eq(errors)
    end

    it 'must have a unique name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
