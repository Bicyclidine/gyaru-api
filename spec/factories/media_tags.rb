# frozen_string_literal: true

FactoryBot.define do
  factory :media_tag do
    association :media, strategy: :build
    association :tag, strategy: :build
  end
end
