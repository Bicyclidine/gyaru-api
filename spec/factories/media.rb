# frozen_string_literal: true

FactoryBot.define do
  factory :media do
    kind { 'anime' }
    status { 'unreleased' }

    after(:build) do |media|
      next if media.names.present?

      media.names << build(:media_name, media: media)
    end
  end
end
