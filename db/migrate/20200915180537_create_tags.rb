# frozen_string_literal: true

# Create a list of tagsfor media, such as "Comedy", "Isekai", etc.
class CreateTags < ActiveRecord::Migration[6.0]
  def change
    create_table :tags do |t|
      t.string :name

      t.timestamps
    end
    add_index :tags, :name, unique: true
  end
end
