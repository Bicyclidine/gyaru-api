# frozen_string_literal: true

# Copy over any existing media names into the new media names table instead.
class CopyNameFromMedia < ActiveRecord::Migration[6.0]
  ROMAJI = 2 # Enum value.

  # Temporary stand-in for the Media model.
  class MediaTable < ActiveRecord::Base
    self.table_name = 'media'
  end

  # Temporary stand-in for the MediaName model.
  class MediaNameTable < ActiveRecord::Base
    self.table_name = 'media_names'
  end

  def up
    MediaTable.all.each do |media|
      next if MediaNameTable.find_by(media_id: media.id) # Name already exists.

      MediaNameTable.create!(
        media_id: media.id, text: media.name, language: 'jp', variant: ROMAJI
      )
    end
  end

  def down
    MediaTable.all.each do |media|
      media_name = MediaNameTable.find_by(media_id: media.id)
      media.update!(name: media_name&.text)
    end
  end
end
