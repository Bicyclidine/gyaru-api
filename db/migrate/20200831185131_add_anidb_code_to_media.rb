# frozen_string_literal: true

# To support list import/export, we need to know the IDs used by other sites:
class AddAnidbCodeToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :anidb_code, :string
  end
end
