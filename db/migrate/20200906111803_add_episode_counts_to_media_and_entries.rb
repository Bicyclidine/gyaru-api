# frozen_string_literal: true

# We want to track episode/chapter/volume counts, so...
class AddEpisodeCountsToMediaAndEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :status, :integer

    add_column :media, :episodes, :integer # or chapters.
    add_column :media, :volumes, :integer

    add_column :list_entries, :episodes, :integer # or chapters.
    add_column :list_entries, :volumes, :integer
  end
end
