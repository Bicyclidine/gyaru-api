# frozen_string_literal: true

# See https://github.com/waiting-for-dev/devise-jwt
class CreateDenyList < ActiveRecord::Migration[6.0]
  def change
    create_table :jwt_denylist do |t|
      t.string :jti, null: false
      t.datetime :exp, null: false
    end
    add_index :jwt_denylist, :jti
  end
end
