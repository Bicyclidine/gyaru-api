# frozen_string_literal: true

# Allows users to flag posts for the attention of administrators.
class CreateUserReports < ActiveRecord::Migration[6.0]
  def change
    create_table :user_reports do |t|
      t.references :user, index: true
      t.references :status_post, index: true
      t.string :reason

      t.timestamps
    end
    add_index :user_reports, %i[user_id status_post_id], unique: true
  end
end
