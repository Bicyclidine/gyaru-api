# frozen_string_literal: true

# Anilist lets you do this, so we shall too...
class AddNotesToListEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :list_entries, :notes, :string
  end
end
