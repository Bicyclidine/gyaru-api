# frozen_string_literal: true

# Allow media to be marked as containing 18+ content.
class AddAdultToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :adult, :boolean, null: false, default: false
  end
end
