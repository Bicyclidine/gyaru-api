# frozen_string_literal: true

# Allow users to receive notifications when things happen.
class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.references :user, index: true
      t.references :context, index: true, polymorphic: true

      t.timestamps
    end
  end
end
