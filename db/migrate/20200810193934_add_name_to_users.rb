# frozen_string_literal: true

# We don't want everyone's emails visible to just anyone!
class AddNameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :name, :string
    add_index :users, :name, unique: true
  end
end
