# frozen_string_literal: true

# A "like" is basically just a line drawn between a user and a status post.
class CreateLikes < ActiveRecord::Migration[6.0]
  def change
    create_table :likes do |t|
      t.references :user
      t.references :status_post
      t.timestamps
    end
    add_index(:likes, %i[status_post_id user_id], unique: true)
  end
end
